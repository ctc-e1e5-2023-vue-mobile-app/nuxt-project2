function parseJwt(token) {
  const base64 = token.split(".");
  if (!base64[1]) {
    return "";
  }
  return JSON.parse(atob(base64[1]));
}

export default defineNuxtPlugin((nuxtApp) => {
  const auth = {
    payload: {},
    isAuthenticated: false,
    refreshTokenTimeout: null,
    init() {
      console.log("auth init");
      const token = this.getToken();
      if (!token) {
        return;
      }
      this.setToken(token);
      const exp = this?.payload?.exp;
      if (!exp) {
        return;
      }
      const now = Date.now() / 1000;
      // exp - now <= 2 minutes
      if (exp - now <= 2 * 60) {
        console.log("init refreshToken");
        this.refreshToken();
      } else {
        console.log("init startRefreshTokenTimer");
        this.startRefreshTokenTimer();
      }
      console.log("init refreshTokenTimeout", this.refreshTokenTimeout);
    },

    setToken(token) {
      if (!token) {
        // localStorage.removeItem("token");
        this.isAuthenticated = false;
        this.payload = {};
      }
      localStorage.setItem("token", token);
      this.payload = parseJwt(token);
      this.isAuthenticated = true;
    },
    removeToken() {
      localStorage.removeItem("token");
      this.isAuthenticated = false;
      this.payload = {};
    },
    getToken() {
      return localStorage.getItem("token");
    },
    setApiHeaders() {
      const token = this.getToken();
      if (token) {
        nuxtApp.$api.defaults.headers.common[
          "Authorization"
        ] = `Bearer ${token}`;
        nuxtApp.$api.defaults.headers.Authorization = `Bearer ${token}`;
      }
    },
    async refreshToken() {
      console.log("refreshToken");
      this.setApiHeaders();
      let res;
      try {
        res = await nuxtApp.$api.get("/api/auth/refresh");
      } catch (error) {
        console.log(error.message);
        this.startRefreshTokenTimer(1000 * 60 * 1);
      }
      const token = res?.data?.data?.token;
      if (!token) {
        console.log("refreshToken: No token");
        this.startRefreshTokenTimer(1000 * 60 * 1);
        return;
      }
      this.setToken(res.data.data.token);
      this.startRefreshTokenTimer();
    },
    startRefreshTokenTimer(milliseconds = 1000 * 60 * 3) {
      // milliseconds = 1500;
      this.stopRefreshTokenTimer();
      this.refreshTokenTimeout = setTimeout(() => {
        this.refreshToken();
      }, milliseconds);
    },
    stopRefreshTokenTimer() {
      if (this.refreshTokenInterval) {
        console.log("stopRefreshTokenTimer");
        clearTimeout(this.refreshTokenTimeout);
      }
    },
    isTokenExpired() {
      const exp = this.payload?.exp;
      if (!exp) {
        return true;
      }
      const now = Date.now() / 1000;
      return exp < now;
    },
  };

  nuxtApp.hook("app:mounted", () => {
    console.log("app:mounted");
  });
  auth.init();
  nuxtApp.provide("auth", auth);
});
