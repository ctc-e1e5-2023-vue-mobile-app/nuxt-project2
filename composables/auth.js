export const useAuth = () => {
  const nuxtApp = useNuxtApp();
  return nuxtApp.$auth;
};
