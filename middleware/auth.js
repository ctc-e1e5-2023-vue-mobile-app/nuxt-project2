export default defineNuxtRouteMiddleware((to, from) => {
  console.log("auth middleware");

  const auth = useAuth();
  const token = auth.getToken();
  if (!token) {
    return navigateTo("/login");
  }
  if (auth.isTokenExpired()) {
    return navigateTo("/login");
  }
});
